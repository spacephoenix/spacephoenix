﻿using UnityEngine;
using System.Collections;

public class Movie : MonoBehaviour {

	private bool pauseEnabled = false;
	private bool showGraphicsDropDown = false;
	private AudioSource audio;

	void Start () {
		((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
		audio = GetComponent<AudioSource> ();
		pauseEnabled = false;
		Cursor.visible = false;
	}


	void Update(){
		if(Input.GetKeyDown("escape")){

			if(pauseEnabled == true){
				pauseEnabled = false;
				((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
				audio.Play();
				Cursor.visible = false;
			}		
			else{
				pauseEnabled = true;
				((MovieTexture)GetComponent<Renderer>().material.mainTexture).Pause();
				audio.Pause();
				Cursor.visible = true;
			}
		}
		if(!((MovieTexture)GetComponent<Renderer>().material.mainTexture).isPlaying && !pauseEnabled)
			Application.LoadLevel("Menu 3D");
	}

	void OnGUI(){
		if(pauseEnabled == true){

			if(GUI.Button(new Rect(Screen.width /2 - 100,Screen.height /2 - 50,250,50), "Main Menu"))
				Application.LoadLevel("Menu 3D");

			if (GUI.Button (new Rect (Screen.width /2 - 100,Screen.height /2 + 50,250,50), "Quit Game"))
				Application.Quit();
			
			if (GUI.Button (new Rect (Screen.width /2 - 100,Screen.height /2,250,50), "Restart")){
				((MovieTexture)GetComponent<Renderer>().material.mainTexture).Stop();
				audio.Stop();
				((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
				audio.Play();
				Cursor.visible = false;
				pauseEnabled = false;
			}
		}
	}
}
