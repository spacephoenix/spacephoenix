﻿using UnityEngine;
using System.Collections;

public class ISSTracker : MonoBehaviour {

	private Transform ISS;
	private Rigidbody rigid;
	public float speed;
	private EvasiveManeuver evasive;
	private WeaponController weapon;
	private GameController controller;
	
	void Awake (){
		GameObject go = GameObject.FindGameObjectWithTag ("ISS");
		if(go != null)
			ISS = go.transform;
		rigid = GetComponent<Rigidbody> ();
		evasive = GetComponent<EvasiveManeuver> ();
		weapon = GetComponent<WeaponController> ();
		controller = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
		if (controller.inLevelTwo)
			Track ();
	}
	
	public void Track(){
		evasive.enabled = false;
		rigid.velocity = Vector3.zero;
		weapon.enabled = false;
		GetComponent<ISSTracker>().enabled = true;
	}
	
	void Update (){
		if (ISS != null) {
			transform.LookAt (ISS.position);
			if (Vector3.Distance (ISS.position, transform.position) > 40) {
				rigid.velocity = transform.forward * speed;
				weapon.enabled = false;
			} else {
				weapon.enabled = true;
				rigid.velocity = Vector3.zero;
			}
		} else {
			//weapon.CancelFire();
			weapon.enabled = false;
			rigid.velocity = Vector3.zero;
		}
		
	}
}
