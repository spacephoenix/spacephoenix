﻿using UnityEngine;
using System.Collections;

public class BoundaryDestroyer : MonoBehaviour {

	private GameController controller;

	void Awake(){
		GameObject go = GameObject.FindWithTag ("GameController");
		controller = go.GetComponent<GameController> ();
	}

	void OnTriggerExit(Collider other) {
		//if (other.tag != "Player" && other.tag != "Pulse" && other.tag != "Trail" ) {
		if(other.tag == "Enemy"){
			if(controller.inLevelTwo){
				try{
					DestroyByContact destroy = other.gameObject.GetComponent <DestroyByContact> ();
					if (destroy != null)
						destroy.FastDead ();
					else{
						destroy = other.gameObject.GetComponentInParent <DestroyByContact> ();
						if(destroy != null)
							destroy.FastDead ();
					}
				}catch(System.Exception e){
	
				}
			}
			else
				Destroy (other.gameObject);
		}
		else if(other.tag == "Player"){
			controller.GetBack();
		}
		else if(other.tag == "Squad"){
			Destroy (other.gameObject);
		}
	}


	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player")
			controller.Back ();
	}
}
