﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Loading : MonoBehaviour {
	public Image [] loading;
	private Color flashColour = new Color(1f, 1f, 1f, 1f);
	public GameObject input;

	void Update () {
		if (Application.isLoadingLevel) {
			int i = Random.Range(0, loading.Length);
			loading[i].color = flashColour;
			input.SetActive(false);
		}
	}
}
