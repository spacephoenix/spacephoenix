﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject[] hazards;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public Text text;
	public Text restartT;
	public Text overT;
	public Text battleField;
	private int score = 0;
	private bool restart;
	public bool inLevelOne;
	public bool inLevelTwo;
	public bool onBattle;
	private Controls controls;
	public int scoreRequired;
	public Text objective;
	public Text congrats;
	public GameObject[] blackHoles;
	public float asteroidsRate;
	public Transform asteroidsSpawn;
	public Transform asteroidsSpawn1;
	public Vector2 asteroidsRange;
	public GameObject[] asteroids;
	public GameObject[] rails;
	public GameObject[] blackHoles2;
	public AudioClip deadSong;
	public GameObject defendMessage;
	public GameObject enemyMessage;
	public GameObject enemyMessage2;
	private bool aux = true;

	void Start(){
		inLevelOne = true;
		restart = false;
		onBattle = true;
		overT.text = "";
		restartT.text = "";
		text.text = "Score:  " + score;
		controls = GameObject.FindGameObjectWithTag ("Player").GetComponent<Controls> ();
		StartCoroutine(SpawnWaves ());
		StartCoroutine(AsteroidOn ());
		StartCoroutine (Task());
	}

	void Update(){
		if (score >= scoreRequired && inLevelOne) {
			StartCoroutine (Task2 ());
			inLevelOne = false;
		}

		if (restart)
			if (Input.GetKeyDown (KeyCode.R))
				Application.LoadLevel (Application.loadedLevel);
	}

	IEnumerator AsteroidOn(){
		while (inLevelOne) {
			GameObject asteroid = asteroids [Random.Range (0, asteroids.Length)];
			Vector3 asteroidPosition = new Vector3 (Random.Range (-asteroidsRange.x, asteroidsRange.x), 
		                                       Random.Range (-asteroidsRange.y, asteroidsRange.y), 0f);
		
			Instantiate (asteroid, asteroidsSpawn.position + asteroidPosition, Quaternion.identity);
			Instantiate (asteroid, asteroidsSpawn1.position + asteroidPosition, Quaternion.identity);
			yield return new WaitForSeconds(asteroidsRate);
		}
	}

	IEnumerator SpawnWaves(){
		yield return new WaitForSeconds (startWait);

		for (int i = 0; i < blackHoles.Length; i++)
			Instantiate (blackHoles [i], blackHoles [i].transform.position, blackHoles [i].transform.rotation);

		while (inLevelOne) {
			for (int i = 0; i < hazardCount; i++) {
				GameObject hazard = hazards [Random.Range (0, hazards.Length)];

				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x + transform.position.x,
				        spawnValues.x + transform.position.x),Random.Range (-spawnValues.y +  transform.position.y,
				            spawnValues.y + transform.position.y), spawnValues.z + transform.position.z);

				Quaternion spawnRotation = hazard.transform.rotation;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
		}
	}

	public void AddScore(int valor){
		score += valor;
		UpdateScore ();
	}

	void UpdateScore(){
		text.text = "Score:  " + score;
	}

	IEnumerator Task(){
		yield return new WaitForSeconds (5f);
		objective.text = "Get " + scoreRequired + " points";
		yield return new WaitForSeconds (4f);
		objective.text = "";
	}

	IEnumerator Task2(){
		GetComponent<Mover> ().enabled = false;
		objective.text = "Good Job!";
		yield return new WaitForSeconds (5f);
		objective.text = "Enemy Reinforcements Coming!";
		yield return new WaitForSeconds (5f);
		objective.text = "";
		yield return new WaitForSeconds (5f);
		objective.text = "Defend the Space Station";
		yield return new WaitForSeconds (5f);
		objective.text = "";
		defendMessage.SetActive(true);
		enemyMessage.SetActive(true);
		enemyMessage2.SetActive(true);
		yield return new WaitForSeconds (5f);
		for (int i = 0; i < blackHoles2.Length; i++)
			blackHoles2 [i].SetActive (true);

		for (int i = 0; i < rails.Length; i++)
			rails [i].SetActive (false);

		inLevelTwo = true;
		GetComponent<Level2> ().enabled = true;
		GetComponent<Level2> ().StartHordes();
		GetComponentInChildren<ISSHealth> ().ActiveColl();
		yield return new WaitForSeconds (15f);
		defendMessage.SetActive(false);
		enemyMessage.SetActive(false);
		enemyMessage2.SetActive(false);
	}

	public void GameOver(){
		AudioSource audio = GetComponent<AudioSource> ();
		audio.Stop ();
		audio.clip = deadSong;
		audio.Play ();
		audio.volume = 1f;
		overT.text = "Game over!";
		inLevelOne = false;
		inLevelTwo = false;
		restartT.text = "Press R for Restart";
		GetComponent<Level2> ().enabled = false;
		restart = true;
		int tmp = 0;
		string tmp2 = "";
		int tmp3 = 0;
		string tmp4 = "";

		if (score > PlayerPrefs.GetInt ("Player Score 0")) {
			aux = false;
			tmp = PlayerPrefs.GetInt ("Player Score 0", 0);
			tmp2 = PlayerPrefs.GetString ("Name1","Nobody ");
			tmp3 = PlayerPrefs.GetInt ("Player Score 6", 0);
			tmp4 = PlayerPrefs.GetString ("Name2","Nobody ");
			PlayerPrefs.SetInt ("Player Score 0", score);
			PlayerPrefs.SetString ("Name1",PlayerPrefs.GetString("Name", name));
			congrats.text = "Congratulations, you have got the Gold medal!";
			PlayerPrefs.SetInt ("Player Score 6", tmp);
			PlayerPrefs.SetString ("Name2", tmp2);
			PlayerPrefs.SetInt ("Player Score 5", tmp3);
			PlayerPrefs.SetString ("Name3",tmp4);
		} else if (score > PlayerPrefs.GetInt ("Player Score 6") && aux) {
			aux = false;
			tmp = PlayerPrefs.GetInt ("Player Score 6", 0);
			tmp2 = PlayerPrefs.GetString ("Name2","Nobody ");
			PlayerPrefs.SetInt ("Player Score 6", score);
			PlayerPrefs.SetString ("Name2",PlayerPrefs.GetString("Name", name));
			congrats.text = "Congratulations, you have got the Silver medal!";
			PlayerPrefs.SetInt ("Player Score 5", tmp);
			PlayerPrefs.SetString ("Name3",tmp2);
		} else if (score > PlayerPrefs.GetInt ("Player Score 5") && aux) {
			PlayerPrefs.SetInt ("Player Score 5", score);
			PlayerPrefs.SetString ("Name3",PlayerPrefs.GetString("Name", name));
			congrats.text = "Congratulations, you have got the Bronze medal!";
		}
	}

	IEnumerator filedTimer(){
		for(int i = 9; i >= 0; i--){
			if(onBattle)
				break;
			battleField.text = "Get back to the BattleField! " + i;
			if(i == 0){
				controls.GameOver();
				GameOver();
			}
			yield return new WaitForSeconds (1f);
		}
	}

	public void GetBack(){
		battleField.text = "Get back to the BattleField! 9";
		onBattle = false;
		StartCoroutine (filedTimer ());
	}

	public void Back(){
		battleField.text = "";
		onBattle = true;
	}
}