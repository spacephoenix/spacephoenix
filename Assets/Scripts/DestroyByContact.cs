﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {
	public GameObject explotion;
	public int scoreValue;
	public int health;
	public int contactDamage;
	private ShipHealth player;
	private GameController controller;
	private ISSHealth ISS;
	private bool dead;
	private Rigidbody rigid;
	private Counter counter;

	void Awake(){
		controller = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
		GameObject GCO = GameObject.FindWithTag ("ISS");
		if (GCO != null)
			ISS = GCO.GetComponent<ISSHealth> ();
		dead = false;
		rigid = GetComponent<Rigidbody> ();
		counter = GetComponent<Counter>();
	}

	void Start(){
		GameObject GCO = GameObject.FindWithTag ("Player");
		if (GCO != null)
			player = GCO.GetComponent<ShipHealth> ();
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Boundary" || other.tag == "Enemy") {
			return;
		}
		Instantiate (explotion, transform.position, transform.rotation);
		if (other.tag == "Player") {
			player.takeDamage (contactDamage);
			takeDamage (contactDamage);
		}
		else if(other.tag == "ISS"){
			ISS.takeDamage (contactDamage);
			takeDamage (contactDamage);
		}
		else if(other.tag == "Squad"){
			other.gameObject.GetComponent<SquadHealth>().takeDamage(contactDamage);
			takeDamage (contactDamage);
		}else if(other.tag == "Pulse"){
			takeDamage (200);
			rigid.MovePosition(transform.position - transform.forward * 20);
		}

	}

	public void FastDead(){
		if(counter != null)
			counter.Dead();
		Destroy (gameObject);
	}

	public void takeDamage(int damage){
		health -= damage;
		if (health <= 0 && !dead) {
			dead = true;
			Instantiate (explotion, transform.position, transform.rotation);
			controller.AddScore (scoreValue);
			if(counter != null)
				counter.Dead();
			Destroy (gameObject);
		}
	}
}
