using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Controls : MonoBehaviour {

	public float speed;
	public float ztilt;
	public float ytilt;
	public float xtilt;
	public float turbo;
	public float maxYRot;
	public float maxZRot;
	public float camaraTilt;
	public float maxCamaraRot;
	public float fireRate;
	public float dashSpeed;
	public float upDashSpeed;
	public float stabilizator;

	public GameObject secondCamera;
	public GameObject distortion;
	public GameObject tunel;
	
	public Transform camara;

	private float nextFire;
	private float moveHorizontal;
	private float moveVertical;
	private float yrot;
	private float xrot;
	private float zrot;
	private float crot;
	private float speed2;
	private float sign;
	private float stop;

	private bool rotating;
	private bool turn;
	private bool dash;
	private bool evade;
	private bool isDashing;

	private Quaternion r;
	private Quaternion orientator;
	private Quaternion dashZR;
	private Quaternion dashXR;
	private Quaternion camDefRot;

	private Rigidbody rigid;
	public AudioClip horizontalDash;
	public AudioClip verticalDash;
	public AudioClip engines;
	public AudioClip turboEngines;

	private GameObject MainCamera;
	private AudioSource verticalDashSource;
	private AudioSource horizontalDashSource;
	private AudioSource enginesSource;
	private AudioSource turboEnginesSource;

	public Transform audioSourcePrefab;
	public Vector3 warpSize;
	public Vector3 warpPosition;
	public GameObject explosion;
	private GameController controller;


	void Awake(){
		xrot = 0f;
		zrot = 0f;
		crot = 0f;
		sign = 1;
		stop = 1;
		rotating = false;
		turn = false;
		dash = false;
		isDashing = false;
		r = Quaternion.Euler (0f, 0f, 180f);
		dashZR = Quaternion.Euler (0f, 0f, dashSpeed);
		dashXR = Quaternion.Euler (upDashSpeed, 0f, 0f);
		orientator = Quaternion.Euler (0f, 0f, stabilizator);
		rigid = GetComponent<Rigidbody> ();

		verticalDashSource = ((Transform)Instantiate(audioSourcePrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioSource>();
		verticalDashSource.clip = verticalDash;
		verticalDashSource.transform.parent = transform;

		horizontalDashSource = ((Transform)Instantiate(audioSourcePrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioSource>();
		horizontalDashSource.clip = horizontalDash;
		horizontalDashSource.transform.parent = transform;

		enginesSource = ((Transform)Instantiate(audioSourcePrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioSource>();
		enginesSource.clip = engines;
		enginesSource.loop = true;
		enginesSource.transform.parent = transform;

		turboEnginesSource = ((Transform)Instantiate(audioSourcePrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioSource>();
		turboEnginesSource.clip = turboEngines;
		turboEnginesSource.loop = true;
		turboEnginesSource.transform.parent = transform;

		controller = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
	}

	void Start(){
		yrot = transform.localRotation.eulerAngles.y;
		MainCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		camDefRot = MainCamera.transform.localRotation;
		enginesSource.Play ();
		turboEnginesSource.Play ();
		turboEnginesSource.Pause ();
	}

	void Update(){
		if (isDashing)
			return;

		if (Input.GetButton ("Jump"))
			dash = true;
		else if (Input.GetButton ("evasive"))
			evade = true;
		else {
			dash = false;
			evade = false;
		}

		if (Input.GetKeyDown (KeyCode.R))
			if (stop == 1)
				stop = 0;
			else
				stop = 1;

		moveHorizontal = Input.GetAxis ("Horizontal");
		moveVertical = Input.GetAxis ("Vertical");

		if (moveHorizontal != 0) {
			rotating = false;
			if (zrot > maxZRot && moveHorizontal > 0)
				zrot += -moveHorizontal * ztilt;
			else if (zrot < -maxZRot && moveHorizontal < 0)
				zrot += -moveHorizontal * ztilt;
			else if(zrot <= maxZRot && zrot >= -maxZRot)
				zrot += -moveHorizontal * ztilt;

			if (yrot > maxYRot && moveHorizontal > 0)
				yrot += -moveHorizontal * ytilt;
			else if (yrot < -maxYRot && moveHorizontal < 0)
				yrot += -moveHorizontal * ytilt;
			else
				yrot += -moveHorizontal * ytilt;

			if (crot > maxCamaraRot && moveHorizontal < 0)
				crot += moveHorizontal * camaraTilt;
			else if (crot < -maxCamaraRot && moveHorizontal > 0)
				crot += moveHorizontal * camaraTilt;
			else if(crot <= maxCamaraRot && crot >= -maxCamaraRot)
				crot += moveHorizontal * camaraTilt;

			if(!controller.inLevelTwo)
				yrot = Mathf.Clamp (yrot, -70f, 70f);
			
			camara.localRotation = Quaternion.Euler (camDefRot.eulerAngles.x, crot + camDefRot.eulerAngles.y, -zrot);
		}
		else if (moveVertical != 0) {
			if(Mathf.Abs(zrot) > 0.5)
				zrot /= 1.1f;
			else
				zrot = 0;
			if(Mathf.Abs(crot) > 0.5)
				crot /= 1.1f;
			else
				crot = 0;

			xrot += moveVertical * xtilt * sign;
			if(!controller.inLevelTwo)
				xrot = Mathf.Clamp (xrot, -70f, 70f);
			rotating = true;
			camara.localRotation = Quaternion.Euler (camDefRot.eulerAngles.x, crot + camDefRot.eulerAngles.y, -zrot);
		}else {
			if(Mathf.Abs(zrot) > 0.5)
				zrot /= 1.1f;
			else
				zrot = 0;
			if(Mathf.Abs(crot) > 0.5)
				crot /= 1.1f;
			else
				crot = 0;

			rotating = false;

			camara.localRotation = Quaternion.Euler (camDefRot.eulerAngles.x, crot + camDefRot.eulerAngles.y, -zrot);
		}

		if (Input.GetButton ("Turbo")) {
			enginesSource.Pause ();
			turboEnginesSource.UnPause ();
			speed2 = turbo;
			if(stop != 0){
			distortion.SetActive(true);
			tunel.transform.localScale = new Vector3(1f,1f,1f);
			tunel.transform.localPosition = new Vector3(0f,0f,0f);
			}
		} else {
			enginesSource.UnPause ();
			turboEnginesSource.Pause ();
			speed2 = speed;
			distortion.SetActive(false);
			if(stop == 0)
				tunel.SetActive(false);
			else{
				tunel.SetActive(true);
				tunel.transform.localScale = warpSize;
				tunel.transform.localPosition = warpPosition;
			}
		}
		transform.localRotation = Quaternion.Euler (-xrot, -yrot, zrot);
		if (Time.timeScale > 0) {
			rigid.AddForce (transform.forward * speed2 * stop);
		}
	}

	void LateUpdate(){
		if(!controller.inLevelTwo){
			Vector3 aux = new Vector3(Mathf.Clamp (transform.position.x, -8f, 8f), 
			                          Mathf.Clamp (transform.position.y, -8f, 8f), 
			Mathf.Clamp(transform.position.z,controller.transform.position.z - 40, controller.transform.position.z + 70));
			transform.position = aux;
		}

		if (!isDashing) {
			if (!turn) {
				if (!rotating && transform.localRotation.eulerAngles.z >= 174 && transform.localRotation.eulerAngles.z <= 186) {
					StartCoroutine(Orientate());
					turn = true;
					sign = -1;
				}
			} else {
				if (!rotating && transform.localRotation.eulerAngles.z <= 8 && transform.localRotation.eulerAngles.z >= -8) {
					turn = false;
					transform.localRotation *= r;
					StartCoroutine(Orientate());
					sign = 1;
				} else
					transform.localRotation *= r;
			}
			if (dash) {
				if (Input.GetKeyUp (KeyCode.LeftArrow)) {
					horizontalDashSource.Play();
					StartCoroutine (LeftDash ());
				} else if (Input.GetKeyUp (KeyCode.RightArrow)) {
					horizontalDashSource.Play();
					StartCoroutine (RightDash ());
				}
			}
			if(evade && controller.inLevelTwo){
				if (Input.GetKeyUp (KeyCode.UpArrow)){
					verticalDashSource.Play();
					StartCoroutine (UpDash ());
				}
				else if (Input.GetKeyUp (KeyCode.DownArrow)){
					verticalDashSource.Play();
					StartCoroutine (DownDash ());
				}
				dash = false;
			}
		}
	}

	IEnumerator Orientate(){
		isDashing = true;
		rotating = true;
		float aux = 180f / stabilizator;
		for(int i = 0; i < aux; i++){
			Quaternion.Lerp(transform.localRotation, transform.localRotation *= orientator, Time.deltaTime * 5f);
			yield return new WaitForSeconds (0.01f);
		}
		isDashing = false;
	}

	IEnumerator LeftDash(){
		secondCamera.SetActive (true);
		secondCamera.transform.position = MainCamera.transform.position;
		secondCamera.transform.rotation = MainCamera.transform.rotation;
		MainCamera.SetActive (false);
		isDashing = true;
		float aux2 = 360f / dashSpeed;
		Vector3 aux = -2f/aux2 * transform.right;
		for(int i = 0; i < aux2; i++){
			Quaternion.Lerp(transform.localRotation, transform.localRotation *= dashZR, Time.deltaTime * 5f);
			secondCamera.transform.position += aux/1.5f + transform.forward * 0.01f;
			transform.position += aux;
			yield return new WaitForSeconds (0.01f);
		}
		secondCamera.transform.position += aux/1.5f + transform.forward * 0.01f;
		isDashing = false;
		if (turn)
			transform.localRotation *= r;
		MainCamera.SetActive (true);
		secondCamera.SetActive (false);
	}

	IEnumerator RightDash(){
		secondCamera.SetActive (true);
		secondCamera.transform.position = MainCamera.transform.position;
		secondCamera.transform.rotation = MainCamera.transform.rotation;
		MainCamera.SetActive (false);
		isDashing = true;
		float aux2 = 360f / dashSpeed;
		Vector3 aux = 2f/aux2 * transform.right;
		for(int i = 0; i < aux2; i++){
			Quaternion.Lerp(transform.localRotation, transform.localRotation *= Quaternion.Inverse(dashZR), Time.deltaTime * 5f);
			secondCamera.transform.position += aux/1.5f + transform.forward * 0.01f;
			transform.position += aux;
			yield return new WaitForSeconds (0.01f);
		}
		secondCamera.transform.position += aux/1.5f + transform.forward * 0.01f;
		isDashing = false;
		if(turn)
			transform.localRotation *= r;
		MainCamera.SetActive (true);
		secondCamera.SetActive (false);
	}

	IEnumerator UpDash(){
		secondCamera.SetActive (true);
		secondCamera.transform.position = MainCamera.transform.position;
		secondCamera.transform.rotation = MainCamera.transform.rotation;
		MainCamera.SetActive (false);
		isDashing = true;
		float aux2 = 180f / upDashSpeed;
		Vector3 aux = 2f/aux2 * transform.up;
		Quaternion aux3 = Quaternion.Euler (0f, upDashSpeed, 0f);
		Vector3 aux4 = 3.5f/aux2 * transform.forward;
		for(int i = 0; i < aux2; i++){
			Quaternion.Lerp(transform.localRotation, transform.localRotation *= Quaternion.Inverse (dashXR), Time.deltaTime * 5f);
			Quaternion.Lerp(secondCamera.transform.rotation, secondCamera.transform.rotation *= aux3, Time.deltaTime * 5f);
			transform.position += aux;
			secondCamera.transform.position += aux4;
			secondCamera.transform.position += aux/1.1f;
			xrot -= upDashSpeed;
			yield return new WaitForSeconds (0.01f);
		}
		secondCamera.transform.position += aux4;
		secondCamera.transform.position += aux/1.1f;
		isDashing = false;
		transform.localRotation *= r;
		MainCamera.SetActive (true);
		secondCamera.SetActive (false);
	}

	IEnumerator DownDash(){
		secondCamera.SetActive (true);
		secondCamera.transform.position = MainCamera.transform.position;
		secondCamera.transform.rotation = MainCamera.transform.rotation;
		MainCamera.SetActive (false);
		isDashing = true;
		float aux2 = 180f / upDashSpeed;
		Vector3 aux = -2f/aux2 * transform.up;
		Quaternion aux3 = Quaternion.Euler (0f, upDashSpeed, 0f);
		Vector3 aux4 = 3.5f/aux2 * transform.forward;
		for(int i = 0; i < aux2; i++){
			Quaternion.Lerp(transform.localRotation, transform.localRotation *= dashXR, Time.deltaTime * 5f);
			Quaternion.Lerp(secondCamera.transform.rotation, secondCamera.transform.rotation *= Quaternion.Inverse(aux3), Time.deltaTime * 5f);
			transform.position += aux;
			secondCamera.transform.position += aux4;
			secondCamera.transform.position += aux/1.1f;
			xrot += upDashSpeed;
			yield return new WaitForSeconds (0.01f);
		}
		secondCamera.transform.position += aux4;
		secondCamera.transform.position += aux/1.1f;
		isDashing = false;
		transform.localRotation *= r;
		MainCamera.SetActive (true);
		secondCamera.SetActive (false);
	}

	public void GameOver(){
		secondCamera.transform.position = MainCamera.transform.position;
		secondCamera.transform.rotation = MainCamera.transform.rotation;
		MainCamera.SetActive (false);
		secondCamera.SetActive (true);
		Instantiate (explosion, transform.position, transform.rotation);
		Destroy (gameObject);
	}
}
