using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GetHighscore : MonoBehaviour {

	public Text scoreText1;
	public Text scoreText2;
	public Text scoreText3;

	void Start(){
		scoreText1.text = "1st Position:    "+PlayerPrefs.GetString ("Name1","Nobody ") +"        "+ PlayerPrefs.GetInt ("Player Score 0",0);
		scoreText2.text = "2nd Position:    "+PlayerPrefs.GetString ("Name2", "Nobody ")+"        " + PlayerPrefs.GetInt("Player Score 6",0);
		scoreText3.text = "3th Position:    "+PlayerPrefs.GetString ("Name3", "Nobody ")+"        " + PlayerPrefs.GetInt("Player Score 5",0);
	}
}