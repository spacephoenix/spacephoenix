﻿using UnityEngine;
using System.Collections;

public class Compass : MonoBehaviour {

	private Transform player;
	private GameController controller;
	
	void Start () {
		player = GameObject.FindWithTag ("Player").GetComponent<Transform>();
		controller = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
	}

	void LateUpdate () {
		if (player != null) {
			if(controller.inLevelTwo)
				transform.rotation = Quaternion.Euler (0f, 0f, player.rotation.eulerAngles.y + 90);
			else
				transform.rotation = Quaternion.Euler (0f, 0f, player.rotation.eulerAngles.y);
		}
	}
}
