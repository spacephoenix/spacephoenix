﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {

	public float speed;
	private Rigidbody rigid;

	void Start(){
		rigid = GetComponent<Rigidbody> ();
	}

	void Update(){
		if (Time.timeScale > 0) {
			rigid.AddForce (transform.forward * speed);
		}
	}
}
