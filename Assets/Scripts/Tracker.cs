﻿using UnityEngine;
using System.Collections;

public class Tracker : MonoBehaviour {
	
	private Transform player;
	private Rigidbody rigid;
	public float speed;
	private EvasiveManeuver evasive;
	private WeaponController weapon;
	private GameController controller;

	void Awake (){
		rigid = GetComponent<Rigidbody> ();
		evasive = GetComponent<EvasiveManeuver> ();
		weapon = GetComponent<WeaponController> ();
		controller = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
		if (controller.inLevelTwo)
			Track ();
	}

	void Start(){
			player = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	public void Track(){
		GetComponent<Tracker>().enabled = true;
		evasive.enabled = false;
		rigid.velocity = Vector3.zero;
		weapon.enabled = false;
	}
	
	void Update (){
		if (player != null && controller.onBattle) {
			transform.LookAt (player.position);
			if (Vector3.Distance (player.position, transform.position) > 20) {
				rigid.velocity = transform.forward * speed;
				weapon.enabled = false;
			} else {
				weapon.enabled = true;
				rigid.velocity = Vector3.zero;
			}
		}else {
			weapon.CancelFire();
			weapon.enabled = false;
			rigid.velocity = Vector3.zero;
		}
	}
}
