﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorMouse : MonoBehaviour {
	public Texture2D cursorTexture;
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;
	public int Save;
	public static int save;

	void OnMouseEnter(){
		Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);

	}
	void OnMouseExit(){
		Cursor.SetCursor (null, Vector2.zero, cursorMode);
	}
	void OnMouseDown (){
		save = Save;
		Application.LoadLevel ("_Scene");
	}
}
