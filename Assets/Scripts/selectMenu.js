﻿#pragma strict
var pauseMenuFont : Font;
private var pauseEnabled = false;

function Start(){
	pauseEnabled = false;
}

function Update(){

	if(Input.GetKeyDown("escape")){
	
		if(pauseEnabled == true){
			pauseEnabled = false;
			Time.timeScale = 1;
		}		
		else{
			pauseEnabled = true;
			Time.timeScale = 0;
		}
	}
}

private var showGraphicsDropDown = false;

function OnGUI(){

	GUI.skin.box.font = pauseMenuFont;
	GUI.skin.button.font = pauseMenuFont;

	if(pauseEnabled == true){
		
		if(GUI.Button(Rect(Screen.width /2 - 100,Screen.height /2 - 50,250,50), "Main Menu")){
			Application.LoadLevel("Menu 3D");
			pauseEnabled = false;
		}
		
		if (GUI.Button (Rect (Screen.width /2 - 100, Screen.height /2,250,50), "Quit Game"))
			Application.Quit();
	}
}