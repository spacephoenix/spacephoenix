﻿using UnityEngine;	
using System.Collections;
using UnityEngine.UI;

public class ShipsDiseable : MonoBehaviour {


	public GameObject andromeda;
	public GameObject venus;
	public GameObject earth;
	public GameObject rogue;
	static int ActualShip;
	public Image[] photo;
	private Color flashColour = new Color(1f, 1f, 1f, 1f);
	// Use this for initialization
//	void Start () {
//		andromeda = GameObject.Find ("Andromeda");
//		venus = GameObject.Find ("Venus");
//		earth = GameObject.Find ("Earth");
//		rogue = GameObject.Find ("Rogue");
//	}


	void Awake () {
		if (ColorMouse.save == 1) {
			andromeda.SetActive (true);
			venus.SetActive (false);
			earth.SetActive (false);
			rogue.SetActive (false);
			photo[3].color = flashColour;
		} 
		else if (ColorMouse.save == 3) {
			andromeda.SetActive (false);
			venus.SetActive (true);
			earth.SetActive (false);
			rogue.SetActive (false);
			photo[1].color = flashColour;
		}
		else if (ColorMouse.save == 2) {
			andromeda.SetActive (false);
			venus.SetActive (false);
			earth.SetActive (true);
			rogue.SetActive (false);
			photo[2].color = flashColour;
		} 
		else if (ColorMouse.save == 4) {
			andromeda.SetActive (false);
			venus.SetActive (false);
			earth.SetActive (false);
			rogue.SetActive (true);
			photo[0].color = flashColour;
		}
	}
}
