﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class ShipHealth : MonoBehaviour {

	public int health;
	private int defHealth;
	public Slider healthSlider;
	public Image [] broken;
	private Color flashColour = new Color(1f, 1f, 1f, 1f);
	private Color flashColour2 = new Color(1f, 1f, 1f, 0f);
	private GameController controller;
	private Controls controls;
	public Slider powerSlider;
	public float powerRate;
	private float nextPower;
	public bool inPower;
	private GameObject pulse;
	private GameObject trail;

	void Awake(){
		trail = GameObject.FindWithTag("Trail");
		pulse = GameObject.FindWithTag("Pulse");
		trail.SetActive (false);
		pulse.SetActive (false);
		healthSlider.maxValue = health;
		healthSlider.value = health;
		defHealth = health;
		controller = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
		controls = GetComponent<Controls> ();
		inPower = false;
	}

	void Update(){
		if (Input.GetKeyUp (KeyCode.S)) {
			if(inPower){
				inPower = false;
				pulse.SetActive(false);
				trail.SetActive(false);
			}
			else{
				inPower = true;
				pulse.SetActive(true);
				trail.SetActive(true);
			}
		}
		if (!inPower) {
			if (Time.time > nextPower && powerSlider.value <= 20) {
				nextPower = Time.time + powerRate;
				powerSlider.value ++;
			}
		} else {
			if (Time.time > nextPower){
				nextPower = Time.time + powerRate;
				if(powerSlider.value > 0)
					powerSlider.value --;
				else{
					pulse.SetActive(false);
					trail.SetActive(false);
					inPower = false;
				}
			}
		}
		
	}

	public void takeDamage(int damage){
		if (!inPower) {
			health -= damage;
			healthSlider.value = health;
			if (health <= 0) {
				broken [0].color = flashColour;
				broken [1].color = flashColour;
				broken [2].color = flashColour;
				broken [3].color = flashColour;
				controls.GameOver ();
				controller.GameOver ();
			} else if (health <= 100) { 
				broken [0].color = flashColour;
				broken [1].color = flashColour;
				broken [2].color = flashColour;
				broken [3].color = flashColour;
			} else if (health <= 150) {
				broken [1].color = flashColour;
				broken [2].color = flashColour;
				broken [3].color = flashColour;
			} else if (health <= 200) {
				broken [2].color = flashColour;
				broken [3].color = flashColour;
			} else if (health <= 300) 
				broken [3].color = flashColour;
		}
		
	}
	
	public void ResetHelath(){
		health = defHealth;
		healthSlider.value = health;
		broken [0].color = flashColour2;
		broken [1].color = flashColour2;
		broken [2].color = flashColour2;
		broken [3].color = flashColour2;
	}
}
