﻿using UnityEngine;
using System.Collections;

public class SquadHealth : MonoBehaviour {

	public GameObject explotion;
	public int health;
	
	public void takeDamage(int damage){
		health -= damage;
		if (health <= 0) {
			Instantiate (explotion, transform.position, transform.rotation);
			Destroy (gameObject, 4f);
		}
	}
}
