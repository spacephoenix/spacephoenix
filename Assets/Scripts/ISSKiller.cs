﻿using UnityEngine;
using System.Collections;

public class ISSKiller : MonoBehaviour {

	private Transform ISS;
	private Rigidbody rigid;
	public float speed;
	private EvasiveManeuver evasive;
	private WeaponController weapon;
	private Level2 level2;
	private GameController controller;
	public int wave;
	public int anotherWave;

	void Awake (){
		GameObject go = GameObject.FindGameObjectWithTag ("ISS");
		if(go != null)
			ISS = go.transform;
		rigid = GetComponent<Rigidbody> ();
		evasive = GetComponent<EvasiveManeuver> ();
		weapon = GetComponent<WeaponController> ();
		go = GameObject.FindWithTag ("GameController");
		level2 = go.GetComponent<Level2> ();
		controller = go.GetComponent<GameController> ();
		if (controller.inLevelTwo && (level2.getWave() % 5 == wave || level2.getWave() % 5 == anotherWave))
			Track ();
	}
	
	public void Track(){
		evasive.enabled = false;
		rigid.velocity = Vector3.zero;
		weapon.enabled = false;
		GetComponent<Tracker>().enabled = false;
		GetComponent<ISSKiller>().enabled = true;
	}
	
	void Update (){
		if (ISS != null) {
			transform.LookAt (ISS.position);
			if (Vector3.Distance (ISS.position, transform.position) > 40) {
				rigid.velocity = transform.forward * speed;
				weapon.enabled = false;
			} else {
				weapon.enabled = true;
				rigid.velocity = Vector3.zero;
			}
		} else {
			//weapon.CancelFire();
			weapon.enabled = false;
			rigid.velocity = Vector3.zero;
		}
		
	}
}
