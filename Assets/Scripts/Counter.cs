﻿using UnityEngine;
using System.Collections;

public class Counter : MonoBehaviour {
	
	Level2 level;
	GameController controller;
	bool wave;
	GameObject GO;

	void Awake (){
		GO = GameObject.FindWithTag ("GameController");
		level = GO.GetComponent<Level2> ();
		controller = GO.GetComponent<GameController>();
		if (controller.inLevelTwo)
			wave = true;
	}

	public void Dead(){
		if(wave)
			level.EnemyDown ();
	}
}
