﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour
{
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	public float delay;
	public bool fire = true;

	void Start (){
		StartCoroutine (OnFire ());
	}

	IEnumerator OnFire(){
		yield return new WaitForSeconds (10f);
		InvokeRepeating ("Fire", delay, fireRate);
	}

	void Fire (){
		Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
		GetComponent<AudioSource>().Play();
	}

	void OnDisable(){
		CancelInvoke ("Fire");
	}

	void OnEnable(){
		if (fire)
			fire = false;
		else
			InvokeRepeating ("Fire", delay, fireRate);
	}

	public void CancelFire(){
		CancelInvoke ("Fire");
	}
}
