﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class Name : MonoBehaviour {
	
	string name = "Nobody";
	public Text text;

	void Start(){
		PlayerPrefs.SetString("Name", name);
	}

	public void Save(){
		name = text.text;
		PlayerPrefs.SetString("Name", name);
	}	
}