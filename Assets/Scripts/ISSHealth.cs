﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ISSHealth : MonoBehaviour {

	public GameObject explotion;
	public int health;
	public MeshCollider cool;
	private GameController controller;
	private Controls controls;
	public Text objective;
	public GameObject healthBar;
	private float scale;
	public float barScale;
	public GameObject camara;

	void Start(){
		GameObject go = GameObject.FindWithTag ("GameController");
		controller = go.GetComponent<GameController> ();
		go = GameObject.FindWithTag ("Player");
		controls = go.GetComponent<Controls> ();
		scale = health / barScale;
		healthBar.SetActive (false);
	}

	public void ActiveColl(){
		cool.enabled = true;
		healthBar.SetActive (true);
	}

	public void takeDamage(int damage){
		health -= damage;
		barScale -= damage / scale;
		healthBar.transform.localScale = new Vector3(barScale,0.4f,1f);
		if (health <= 0) {
			healthBar.transform.localScale = Vector3.zero;
			Instantiate (explotion, transform.position, transform.rotation);
			objective.text = "The Station was destroyed!";
			if(controls != null)
				controls.GameOver();
			controller.GameOver ();
			camara.transform.LookAt(transform);
			Destroy (gameObject, 3f);
		} else if (health >= 4600 && health <= 4650) {
			StartCoroutine(Task ());
		}
	}

	IEnumerator Task(){
		objective.text = "The ISS is taking heavy damage!";
		yield return new WaitForSeconds (10f);
		objective.text = "";
	}
}
