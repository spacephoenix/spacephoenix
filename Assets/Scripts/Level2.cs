﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class Level2 : MonoBehaviour {

	public Text objective;
	public Text waveText;
	public Text enemiesText;
	public Transform asteroidsSpawn;
	public Transform asteroidsSpawn1;
	public float waitAsteroids;
	public float asteroidsRate;
	public GameObject[] asteroids;
	public Vector2 asteroidsRange;

	public Transform stalkerSpawn;
	public Vector2 stalkerRange;
	public GameObject stalker;
	public float stalkersRate;

	public Transform pathFinderSpawn;
	public Vector2 pathFinderRange;
	public GameObject pathFinder;
	public float pathFinderRate;

	public Transform trackerSpawn;
	public Vector2 trackerRange;
	public GameObject tracker;
	public float trackerRate;

	public Transform pioneerSpawn;
	public Vector2 pioneerRange;
	public GameObject pioneer;
	public float pioneerRate;

	public Transform engineerSpawn;
	public Vector2 engineerRange;
	public GameObject engineer;
	public float engineerRate;

	private ShipHealth health;
	private int wave;
	private int enemiesNum;

	public int startWait;
	public int trackersNum;
	public int pathFindersNum;
	public int stalkersNum;
	public int pioneersNum;
	public int engineersNum;

	void Start(){
		GameObject GO = GameObject.FindWithTag ("Player");
		if (GO != null)
			health = GO.GetComponent<ShipHealth> ();
		wave = 1;
	}

	public void StartHordes () {
		StartCoroutine (AsteroidOn ());
		StartCoroutine (StartWave (wave));
	}

	IEnumerator StartWave(int num){
		health.ResetHelath ();
		waveText.text = "Wave: " + num.ToString();
		objective.text = "Wave: " + num.ToString();
		yield return new WaitForSeconds (startWait);
		objective.text = "";
		int aux = num % 5;
		switch (aux) {
		case 1:
			pathFindersNum *=2;
			break;
		case 2:
			stalkersNum *=2;
			break;
		case 3:
			trackersNum *=2;
			break;
		case 4:
			pioneersNum *=2;
			break;
		case 0:
			engineersNum *=2;
			break;
		}
		enemiesNum = pathFindersNum + stalkersNum + trackersNum + engineersNum + pioneersNum;
		enemiesText.text = "Enemies: " + enemiesNum.ToString();

		StartCoroutine (TrackersOn (trackersNum));
		StartCoroutine (PathFindersOn (pathFindersNum));
		StartCoroutine (StalkersOn (stalkersNum));
		StartCoroutine (EngineersOn (engineersNum));
		StartCoroutine (PioneersOn (pioneersNum));
	}

	void EngineerOn(){
		GameObject ship = engineer;
		Vector3 shipPosition = new Vector3(Random.Range(-engineerRange.x, engineerRange.x), 
		                                   Random.Range(-engineerRange.y, engineerRange.y), 0f);
		
		Instantiate(ship, engineerSpawn.position + shipPosition, Quaternion.identity);
	}
	
	IEnumerator EngineersOn(int num){
		yield return new WaitForSeconds(engineerRate);
		for (int i = 0; i < num; i++) {
			EngineerOn ();
			yield return new WaitForSeconds(engineerRate);
		}
	}

	void PioneerOn(){
		GameObject ship = pioneer;
		Vector3 shipPosition = new Vector3(Random.Range(-pioneerRange.x, pioneerRange.x), 
		                                   Random.Range(-pioneerRange.y, pioneerRange.y), 0f);
		
		Instantiate(ship, pioneerSpawn.position + shipPosition, Quaternion.identity);
	}
	
	IEnumerator PioneersOn(int num){
		yield return new WaitForSeconds(pioneerRate);
		for (int i = 0; i < num; i++) {
			PioneerOn ();
			yield return new WaitForSeconds(pioneerRate);
		}
	}

	void TrackerOn(){
		GameObject ship = tracker;
		Vector3 shipPosition = new Vector3(Random.Range(-trackerRange.x, trackerRange.x), 
		                                   Random.Range(-trackerRange.y, trackerRange.y), 0f);
		
		Instantiate(ship, trackerSpawn.position + shipPosition, Quaternion.identity);
	}

	IEnumerator TrackersOn(int num){
		yield return new WaitForSeconds(trackerRate);
		for (int i = 0; i < num; i++) {
			TrackerOn ();
			yield return new WaitForSeconds(trackerRate);
		}
	}

	void StalkerOn(){
		GameObject ship = stalker;
		Vector3 shipPosition = new Vector3(Random.Range(-stalkerRange.x, stalkerRange.x), 
		                                   Random.Range(-stalkerRange.y, stalkerRange.y), 0f);

		Instantiate(ship, stalkerSpawn.position + shipPosition, Quaternion.identity);
	}

	IEnumerator StalkersOn(int num){
		yield return new WaitForSeconds(stalkersRate);
		for (int i = 0; i < num; i++) {
			StalkerOn ();
			yield return new WaitForSeconds(stalkersRate);
		}
	}

	void PathFinderOn(){
		GameObject ship = pathFinder;
		Vector3 shipPosition = new Vector3(Random.Range(-pathFinderRange.x, pathFinderRange.x), 
		                                   Random.Range(-pathFinderRange.y, pathFinderRange.y), 0f);

		Instantiate(ship, pathFinderSpawn.position + shipPosition, Quaternion.identity);
	}

	IEnumerator PathFindersOn(int num){
		yield return new WaitForSeconds(pathFinderRate);
		for (int i = 0; i < num; i++) {
			PathFinderOn ();
			yield return new WaitForSeconds(pathFinderRate);
		}
	}

	IEnumerator AsteroidOn(){
		while (true) {
			GameObject asteroid = asteroids [Random.Range (0, asteroids.Length)];
			Vector3 asteroidPosition = new Vector3 (Random.Range (-asteroidsRange.x, asteroidsRange.x), 
		                               Random.Range (-asteroidsRange.y, asteroidsRange.y), 0f);

			Instantiate (asteroid, asteroidsSpawn.position + asteroidPosition, asteroidsSpawn.rotation);
			Instantiate (asteroid, asteroidsSpawn1.position + asteroidPosition, asteroidsSpawn.rotation);
			yield return new WaitForSeconds(asteroidsRate);
		}
	}

	public void EnemyDown(){
		enemiesNum --;
		enemiesText.text = "Enemies: " + enemiesNum.ToString();
		if (enemiesNum <= 0) {
			wave ++;
			StartCoroutine (StartWave(wave));
		}
	}

	public int getWave(){
		return wave;
	}
}
