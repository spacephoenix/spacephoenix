var pauseMenuFont : Font;
private var pauseEnabled = false;

function Start(){
	pauseEnabled = false;
	Time.timeScale = 1;
	AudioListener.volume = 1;
	Cursor.visible = false;
}

function Update(){

	if(Input.GetKeyDown("escape")){
	
		if(pauseEnabled == true){
			pauseEnabled = false;
			Time.timeScale = 1;
			AudioListener.volume = 1;
			Cursor.visible = false;			
		}		
		else{
			pauseEnabled = true;
			AudioListener.volume = 0;
			Time.timeScale = 0;
			Cursor.visible = true;
		}
	}
}

private var showGraphicsDropDown = false;

function OnGUI(){

GUI.skin.box.font = pauseMenuFont;
GUI.skin.button.font = pauseMenuFont;

	if(pauseEnabled == true){
		if(GUI.Button(Rect(Screen.width /2 - 100,Screen.height /2 - 50,250,50), "Main Menu"))
			Application.LoadLevel("Menu 3D");
		
		if (GUI.Button (Rect (Screen.width /2 - 100,Screen.height /2 + 50,250,50), "Quit Game"))
			Application.Quit();
			
		if (GUI.Button (Rect (Screen.width /2 - 100,Screen.height /2,250,50), "Restart"))
			Application.LoadLevel (Application.loadedLevel);
	}
}